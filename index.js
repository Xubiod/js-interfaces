/*

ONLINE EDITION: DO NOTHING ACTUALLY
OFFLINE EDITION: DEVELOP CODE AND GUI
now it doesn't matter, thanks git :)

*/

var main_popup_html;
var remove_debug = false;

// runs on doc load
$(function() {
    console.log(
        "+-------------------------+" + "\n" +
        "|                         |" + "\n" +
        "|   NOTHING TO SEE HERE   |" + "\n" +
        "|  i'm serious just use   |" + "\n" +
        "|    the interface lol    |" + "\n" +
        "+-------------+-----------+" + "\n" +
        "              |            " + "\n" +
        "              |            " + "\n" +
        "              |            ");

    main_popup_html = $(".popup-text").html();
    if (remove_debug) { $(".debug-sidemenu").remove(); }
});

// accepted terms
_267b1b382ace755e4432 = false;

// ----- DOUGHNUT -----
// simplifying nth roots

function doughnut(_x, _y, _z) {
    if (!_267b1b382ace755e4432) { messages.create("you must agree to the popup in order to actually use these tools.", 2000, ".warning-contain"); return false; }
    _u = Math.floor(_y / _z);
    _w = _y % _z;

    //html override
    //old x<sup><sup>u</sup></sup> *( z √x<sup>w</sup> )
    $("#doughnut-ans").html(_x + "<sup><sup>" + _u + "</sup></sup> *( <sup>" + _z + "</sup>√" + _x + "<sup>" + _w + "</sup>)");

    //console
    return (
        _x + "^" + _u + "*(" +
        _z + "√(" + _x + "^" + _w + "))");
}

function doughnut_gui() {
    _x = $("#doughnut-x").val();
    _y = parseInt($("#doughnut-y").val());
    _z = parseInt($("#doughnut-z").val());

    if (_x === "") {
        messages.create("the variable x does not contain anything", 1750, ".error-contain");
        return;
    } else if (isNaN(_y)) {
        messages.create("the variable y is not a number or is empty", 1750, ".error-contain");
        return;
    } else if (isNaN(_z)) {
        messages.create("the variable z is not a number or is empty", 1750, ".error-contain");
        return;
    }

    doughnut(_x, _y, _z);
}

// ----- KREME -----
// simplifying imaginaries to the nth

function kreme(_x) {
    if (!_267b1b382ace755e4432) {
        messages.create("you must agree to the popup in order to actually use these tools.", 2000);
        return false;
    }

    var _u = _x % 4;
    var _k;

    if (_u === 0) {
        _u = 4;
        _k = 1;
    }
    if (_u === 1) { _k = "i"; }
    if (_u === 2) { _k = -1; }
    if (_u === 3) { _k = "-i"; }

    $("#kreme-ans").html("i<sup><sup>" + _x + "</sup></sup>  ->  <strong>i<sup><sup>" + _u + "</sup></sup></strong> or " + _k);

    //console
    return (
        "i^" + _u + " or " + _k);
}

function kreme_gui() {
    _x = parseInt($("#kreme-x").val());

    if (isNaN(_x)) {
        messages.create("the variable x is not a number or is empty", 1750, ".error-contain");
        return;
    } else if (Math.sign(_x) == -1) {
        messages.create("the variable x is not positive", 1750, ".error-contain");
        return;
    } else if (_x == Infinity) {
        messages.create("the result isn't available, as the variable is interpreted as infinity", 1850, ".error-contain");
        return false;
    } else if (_x >= 1e+16) {
        messages.create("the result is probably inaccurate, as the number is interpreted as scientific notation or some digits are ignored (over 16 characters usually)", 3750, ".warning-contain");
    }

    kreme(_x);
}

// ----- COTTON -----
// expontental growth and decay

function cotton(decay, start, percent, input, round) {
    if (decay === true) {
        //exponential decay
        decayRateA = 1-percent;
        decayRateB = Math.pow(decayRateA, input);
            result = start*decayRateB;
        if (round === true) {
            result = Math.round(result);
            $("#cotton-ans").html(result);
            return true;
        } else if (round === false) {
            $("#cotton-ans").html(result);
            return true;
        }
    } else if (decay === false) {
        //exponential growth
        decayRateA = 1+percent;
        decayRateB = Math.pow(decayRateA, input);
        result = start*decayRateB;
        if (round === true) {
            result = Math.round(result);
            $("#cotton-ans").html(result);
            return true;
        } else if (round === false) {
            $("#cotton-ans").html(result);
            return true;
        }
    }
}

function cotton_gui() {
  _decay = $("#cotton-dec").is(":checked");
  _round = $("#cotton-rou").is(":checked");
  
  _start = parseFloat($("#cotton-start").val());
  _percent = parseFloat($("#cotton-percent").val());
  _input = parseFloat($("#cotton-input").val());
  
  cotton(_decay, _start, _percent, _input, _round);
}

// ----- LINEN -----
// linear graph thing

function linen(input, rate, y, round) {
    result = input*rate+y;
    if (round === true) {
        result = Math.round(result);
        $("#linen-ans").html(result);
    } else if (round === false) {
        $("#linen-ans").html(result);
    }
}

function linen_gui() {
  _round = $("#linen-rou").is(":checked");
  
  _input = parseFloat($("#linen-input").val());
  _rate = parseFloat($("#linen-rate").val());
  _y = parseFloat($("#linen-inter").val());
  
  linen(_input, _rate, _y, _round);
}

// ----- STRAW -----
// factoring

function straw(input) {
    var half = Math.floor(input / 2), // Ensures a whole number <= num.
        str = '1', // 1 will be a part of every solution.
        i, j;

    // Determine our increment value for the loop and starting point.
    input % 2 === 0 ? (i = 2, j = 1) : (i = 3, j = 2);

    for (i; i <= half; i += j) {
        input % i === 0 ? str += ', ' + i : false;
    }

    str += ',' + input; // Always include the original number.
    
    $("#straw-ans").html(input + "  =>  <strong>" + str + "</strong>");
}

function straw_gui() {
  _input = parseInt($("#straw-x").val());
  
  if (isNaN(_input)) {
        messages.create("the variable x is not a number or is empty", 1750, ".error-contain");
        return;
    } else if (Math.sign(_input) == -1) {
        messages.create("the variable x is not positive", 1750, ".error-contain");
        return;
    }
  
  straw(_input);
}

// DO NOT MODIFY CODE BELOW THIS COMMENT, ME
// OR ELSE SUFFER THE WRAITH OF POSSIBLE BROKEN CODE

var messages = {
    create: function(input, wait, element) {
    
      $(element)
          .css({ "transfom": "scale(1)"})
          .html(input)
          .css("position", "fixed").css("top", "-32px")
          .animate({ top: 16 })
          .delay(wait)
          .animate({ top: -32 });
    }
}

// END OF PLEASE DO NOT EDIT ZONE

var popup = {
    create_simple: function(input) {
      
        popup.create(input, "close", "popup.close();");
    },
    create: function() {
      //still needs inputs!!!
      try {
      var buttons = (arguments.length - 1) / 2;
      var buttons_html = "";
      var for_count = 0;
      
      for (qxy = 1; for_count < buttons; qxy += 2) {
        for_count++;
        buttons_html = buttons_html + "<a onClick='" + arguments[qxy + 1] + "'>" + arguments[qxy] + "</a> ";
      }
      
        $(".popup-contain").show().css({ 'opacity': 0 }).html(
            "<div class='popup'> " +
            "  <div class='popup-text-contain'>" +
            "    <div class='popup-text'>" +
            "\n" + arguments[0] + "<br><br>" + buttons_html +
            "    </div>" +
            "  </div>" +
            "</div>").animate({ 'opacity': 1 });
      } catch (bugger) {
        messages.create(bugger.message);
      }
    },
    replace_simple : function(input) {
      
        popup.replace(input, "close", "popup.close();");
    },
    replace : function(input) {
      //still needs inputs!!!
      try {
      var buttons = (arguments.length - 1) / 2;
      var buttons_html = "";
      var for_count = 0;
      
      for (qxy = 1; for_count < buttons; qxy += 2) {
        for_count++;
        buttons_html = buttons_html + "<a onClick='" + arguments[qxy + 1] + "'>" + arguments[qxy] + "</a> ";
      }
      
        $(".popup-text").fadeOut("slower", function() {
          $(".popup-text").html(
            "\n" + input + "<br><br>" + buttons_html).fadeIn("slow");
        });
      } catch (bugger) {
        messages.create(bugger.message);
      }
    },
    close: function(callback) {
      
        $(".popup-contain").fadeOut("slower", function() {
            $(".popup-contain").html("");
            callback();
        });
    }
};